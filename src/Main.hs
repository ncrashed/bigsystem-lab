module Main where

import Lens.Micro ((^.), (&), (.~), (%~))
import Lens.Micro.TH (makeLenses)
import Control.Monad (void)
import qualified Graphics.Vty as V

import qualified Brick.Types as T
import Brick.AttrMap
import Brick.Util
import Brick.Types (Widget, ViewportType(Vertical))
import qualified Brick.Focus as F
import qualified Brick.Main as M
import qualified Brick.Widgets.Border as B
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import Brick.Widgets.Core
import Data.Text.Zipper (moveCursor)
import Data.Tuple (swap)

import Graph
import Matrix

data Name = Info | SizeField | InitButton | MatrixField !Row !Column
          deriving (Show, Ord, Eq)

data ClickInfo = ClickInfo {
    _clicked           :: [T.Extent Name]
  , _lastReportedClick :: Maybe (Name, T.Location)
  }

makeLenses ''ClickInfo

data Button n = Button {
    buttonName  :: !n
  , buttonAttr  :: !AttrName
  , buttonLabel :: !String
  }

instance Named (Button n) n where
  getName = buttonName

data InitStage = InitStage {
    _focusRing  :: !(F.FocusRing Name)
  , _sizeField  :: !(E.Editor String Name)
  , _initButton :: !(Button Name)
  }

makeLenses ''InitStage

data MatrixStage = MatrixStage {
    _matrix    :: !(Matrix (Maybe Int))
  }

makeLenses ''MatrixStage

data Stage =
    StageInit   !InitStage
  | StageMatrix !MatrixStage

makeLenses ''Stage

data St = St {
    _clickInfo :: !ClickInfo
  , _stage     :: !Stage
  }

makeLenses ''St

drawUi :: St -> [Widget Name]
drawUi st = case st ^. stage of
  StageInit ist -> [ sizeLayer (st^.clickInfo) ist ]
  StageMatrix mstr -> [ matrixLayer mstr ]

sizeLayer :: ClickInfo -> InitStage -> Widget Name
sizeLayer ci st = C.vCenterLayer $
  C.hCenterLayer (padTopBottom 1 $ str "Введите размер графа:") <=>
  C.hCenterLayer (vLimit 1 $ hLimit 50 sizeEdit) <=>
  C.hCenterLayer btn
  where
    btn = F.withFocusRing (st^.focusRing) (drawButton ci) (st^.initButton)
    sizeEdit = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.sizeField)


drawButton :: ClickInfo -> Bool -> Button Name -> Widget Name
drawButton ci hasFocus Button{..} = let
  wasClicked = hasFocus || (fst <$> ci ^.lastReportedClick) == Just buttonName
  in clickable buttonName $
     withDefAttr buttonAttr $
     B.border $
     padTopBottom 1 $
     padLeftRight (if wasClicked then 2 else 3) $
     str (if wasClicked then "<" <> buttonLabel <> ">" else buttonLabel)

matrixLayer :: MatrixStage -> Widget Name
matrixLayer st = C.vCenterLayer $ str ""


-- buttonLayer :: St -> Widget Name
-- buttonLayer st =
--     C.vCenterLayer $
--       C.hCenterLayer (padBottom (T.Pad 1) $ str "Click a button:") <=>
--       C.hCenterLayer (hBox $ padLeftRight 1 <$> buttons) <=>
--       C.hCenterLayer (padTopBottom 1 $ str "Or enter text and then click in this editor:") <=>
--       C.hCenterLayer (vLimit 3 $ hLimit 50 $ E.renderEditor (str . unlines) True (st^.edit))
--     where
--         buttons = mkButton <$> buttonData
--         buttonData = [ (Button1, "Button 1", "button1")
--                      , (Button2, "Button 2", "button2")
--                      , (Button3, "Button 3", "button3")
--                      ]
--         mkButton (name, label, attr) =
--             let wasClicked = (fst <$> st^.lastReportedClick) == Just name
--             in clickable name $
--                withDefAttr attr $
--                B.border $
--                padTopBottom 1 $
--                padLeftRight (if wasClicked then 2 else 3) $
--                str (if wasClicked then "<" <> label <> ">" else label)

-- proseLayer :: St -> Widget Name
-- proseLayer st =
--   B.border $
--   C.hCenterLayer $
--   vLimit 8 $
--   -- n.b. if clickable and viewport are inverted here, click event
--   -- coordinates will only identify the viewable range, not the actual
--   -- editor widget coordinates.
--   viewport Prose Vertical $
--   clickable Prose $
--   vBox $ map str $ lines (st^.prose)

infoLayer :: St -> Widget Name
infoLayer st = T.Widget T.Fixed T.Fixed $ do
    c <- T.getContext
    let h = c^.T.availHeightL
        msg = case st ^. clickInfo . lastReportedClick of
                Nothing -> "nothing"
                Just (name, T.Location l) -> show name <> " at " <> show l
    T.render $ translateBy (T.Location (0, h-1)) $ clickable Info $
               withDefAttr "info" $
               C.hCenter (str ("Last reported click: " <> msg))

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.MouseDown n _ _ loc) = do
    let T.Location pos = loc
    M.continue $ st & clickInfo . lastReportedClick .~ Just (n, loc)
appEvent st (T.MouseUp _ _ _) = M.continue $ st & clickInfo . lastReportedClick .~ Nothing
appEvent st (T.VtyEvent (V.EvMouseUp _ _ _)) = M.continue $ st & clickInfo . lastReportedClick .~ Nothing
appEvent st (T.VtyEvent (V.EvKey V.KEsc [])) = M.halt st
appEvent st (T.VtyEvent (V.EvKey (V.KChar '\t') [])) = case st ^. stage of
  StageInit si -> do
    let si' = si & focusRing %~ F.focusNext
    M.continue $ st & stage .~ StageInit si'
  _ -> M.continue st
appEvent st (T.VtyEvent (V.EvKey V.KBackTab [])) = case st ^. stage of
  StageInit si -> do
    let si' = si & focusRing %~ F.focusPrev
    M.continue $ st & stage .~ StageInit si'
  _ -> M.continue st
appEvent st@(St _ (StageInit si)) (T.VtyEvent ev) = case F.focusGetCurrent $ si ^. focusRing of
  Just SizeField -> do
    si' <- T.handleEventLensed si sizeField E.handleEditorEvent ev
    M.continue $ st & stage .~ StageInit si'
  _ -> M.continue st
appEvent st _ = M.continue st

aMap :: AttrMap
aMap = attrMap V.defAttr
    [ ("info",                       V.white `on` V.magenta)
    , (E.editAttr,                   V.white `on` V.black)
    , (E.editFocusedAttr,            V.black `on` V.yellow)
    ]

app :: M.App St e Name
app =
    M.App { M.appDraw = drawUi
          , M.appStartEvent = return
          , M.appHandleEvent = appEvent
          , M.appAttrMap = const aMap
          , M.appChooseCursor = M.showFirstCursor
          }

main :: IO ()
main = do
    let buildVty = do
          v <- V.mkVty =<< V.standardIOConfig
          V.setMode (V.outputIface v) V.Mouse True
          return v

    initialVty <- buildVty
    void $ M.customMain initialVty buildVty Nothing app $ St
      (ClickInfo [] Nothing)
      (StageInit $ InitStage {
          _focusRing = F.focusRing [SizeField, InitButton]
        , _sizeField = E.editor SizeField Nothing "17"
        , _initButton = Button InitButton "init_button" "Start"
        })

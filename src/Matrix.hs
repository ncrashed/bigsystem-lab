module Matrix where

import Data.Vector (Vector)

import qualified Data.Vector as V

-- | Count of rows
type Rows = Int
-- | Count of columns
type Columns = Int

-- | Row index 0 started
type Row = Int
-- | Column index 0 started
type Column = Int

-- | Flat index of data in array 0 started
type Index = Int

data Matrix a = Matrix {
    matrixRows    :: !Rows
  , matrixColumns :: !Columns
  , matrixData    :: !(Vector a)
  } deriving (Show)

makeMatrix ::
     Rows -- ^ Row count
  -> Columns -- ^ Column count
  -> (Row -> Column -> a) -- ^ Value setting
  -> Matrix a
makeMatrix rows columns f = Matrix rows columns datum
  where
    datum = V.generate (rows*columns) $ uncurry f . fromIndex columns

-- | Convert flat index to two coordinates (row, column)
fromIndex :: Columns -- ^ Coloumns count
  -> Index -- ^ Flat index
  -> (Row, Column) -- ^ Row and column index
fromIndex columns i = (i `div` columns, i `mod` columns)

-- | Convert row and column indecies to single flat index
toIndex :: Columns  -- ^ Coloumns count
  -> Row -- ^ Row index
  -> Column -- ^ Column index
  -> Index -- ^ Flat index
toIndex columns ri ci = ci + ri * columns

-- | Left fold over matrix with indecies. Strict on accumulator.
foldMatrix :: (Row -> Column -> a -> b -> a) -- ^ Folding function
  -> a -- ^ Initial accumulator value
  -> Matrix b -- ^ Matrix to fold over
  -> a -- ^ Resulted accumulator
foldMatrix f a0 m = V.ifoldl' f' a0 $ matrixData m
  where
    f' !acc !i !b = let
      (ri, ci) = fromIndex (matrixColumns m) i
      in f ri ci acc b

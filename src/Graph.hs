module Graph where

import Data.Vector (Vector)
import Matrix

import qualified Data.Vector as V

type Vert = Int

data Graph a = Graph {
    graphVerts :: !Int
  , graphEdges :: !(Vector (GraphEdge a))
  } deriving (Show)

data GraphEdge a = GraphEdge !Vert !Vert !a
  deriving (Show)

fromMatrix :: Matrix (Maybe a) -> Graph a
fromMatrix m = Graph verts edges
  where
  verts = max (matrixRows m) (matrixColumns m)
  edges = V.fromList $ foldMatrix f [] m
  f !row !col !acc !mv = case mv of
    Nothing -> acc
    Just v -> GraphEdge row col v : acc
